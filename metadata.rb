name 'ihg-appdagent'
maintainer 'IHG'
maintainer_email 'kevin.patel@ihg.com'
license 'All Rights Reserved'
description 'Installs/Configures ihg-appdagent'
long_description 'Installs/Configures ihg-appdagent'
supports 'windows'

version '1.0.30'
chef_version '>= 12.1' if respond_to?(:chef_version)

depends 'windows'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
issues_url 'http://sdlcscm.hiw.com/projects/CHOPS/repos/chef-ihg-appdagent/browse/issues'

# The `source_url` points to the development reposiory for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
source_url 'http://sdlcscm.hiw.com/projects/CHOPS/repos/chef-ihg-appdagent'
