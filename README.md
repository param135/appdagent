# Description

Installs/Configures ihg-appdagent

# Requirements

## Platform:

*No platforms defined*

## Cookbooks:

* windows

# Attributes

* `node['ihg-appdagent']['install_dir']` -  Defaults to `C:\appdynamics`.
* `node['ihg-appdagent']['monitoring_template']` -  Defaults to `[ ... ]`.
* `node['ihg-appdagent']['machineagent']['version']` -  Defaults to `4.5.10.2131`.
* `node['ihg-appdagent']['machineagent']['install_dir']` -  Defaults to `#{node['ihg-appdagent']['install_dir']}/machine_agent/#{node['ihg-appdagent']['machineagent']['version']}`.
* `node['ihg-appdagent']['machineagent']['user']` -  Defaults to `root`.
* `node['ihg-appdagent']['machineagent']['group']` -  Defaults to `root`.
* `node['ihg-appdagent']['machineagent']['extensions']` -  Defaults to `{ ... }`.
* `node['ihg-appdagent']['appagent_dir']` -  Defaults to `#{node['ihg-appdagent']['install_dir']}/oraclejava_agent/#{node['ihg-appdagent']['machineagent']['version']}`.
* `node['ihg-appdagent']['monitor_dir']` -  Defaults to `C:\appdynamics\monitors`.
* `node['ihg-appdagent']['custom_extension']` -  Defaults to `{ ... }`.
* `node['ihg-appdagent']['datacenter']` -  Defaults to `node.attribute?('datacenter') && node['datacenter'].attribute?('name') ? node['datacenter']['name'] : '`.
* `node['ihg-appdagent']['workgroup']` -  Defaults to `node.attribute?('datacenter') && node['datacenter'].attribute?('workgroup') ? node['datacenter']['workgroup'] : '`.
* `node['ihg-appdagent']['function']` -  Defaults to `node.attribute?('datacenter') && node['datacenter'].attribute?('function') ? node['datacenter']['function'] : '`.
* `node['ihg-appdagent']['repo_url']` -  Defaults to `http://sdlcbrm.hiw.com/artifactory/ext-release-local/appdynamics`.
* `node['ihg-appdagent']['machineagent']['pkg']` -  Defaults to `machineagent-bundle-64bit-#{node['os']}-#{node['ihg-appdagent']['machineagent']['version']}.zip`.
* `node['ihg-appdagent']['machineagent']['pkg_url']` -  Defaults to `#{node['ihg-appdagent']['repo_url']}/#{node['ihg-appdagent']['machineagent']['pkg']}`.
* `node['ihg-appdagent']['machineagent']['extension_config']` -  Defaults to `http://sdlcscm.hiw.com/projects/CHSHARED/repos/ihg-appd-extensions/raw`.
* `node['ihg-appdagent']['machineagent']['hierarchy_path']` -  Defaults to `#{node['ihg-appdagent']['datacenter']}|" \`.
* `node['ihg-appdagent']['monitor_bluestripe']` -  Defaults to `if bluestripe_wids.include?(node['ihg-appdagent']['workgroup'])`.
* `node['ihg-appdagent']['controller']['host']` -  Defaults to `ihg-prod.saas.appdynamics.com`.
* `node['ihg-appdagent']['controller']['acc_name']` -  Defaults to `ihg-prod`.
* `node['ihg-appdagent']['controller']['acc_key']` -  Defaults to `e338a1515d7e`.

# Recipes

* ihg-appdagent::default
* ihg-appdagent::deploy_monitors
* ihg-appdagent::destroy_agent
* ihg-appdagent::destroy_monitors
* ihg-appdagent::machine-agent-install-lx
* ihg-appdagent::machine-agent-install-win
* ihg-appdagent::version

# License and Maintainer

Maintainer:: IHG (<kevin.patel@ihg.com>)



License:: All Rights Reserved
