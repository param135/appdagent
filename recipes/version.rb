#
# Cookbook:: ihg-appdagent
# Recipe:: version
#
# Copyright:: 2019, The Authors, All Rights Reserved.

case node['platform_family']
when 'windows'
  unless File.exist?("#{node['ihg-appdagent']['monitor_dir']}/#{node['ihg-appdagent']['custom_extension'].ZipFile}")
    include_recipe 'ihg-appdagent::destroy_monitors'
  end

  unless File.exist?("#{node['ihg-appdagent']['machineagent']['zip_path']}")
    include_recipe 'ihg-appdagent::destroy_agent'
    include_recipe 'ihg-appdagent::destroy_monitors'
  end
end
