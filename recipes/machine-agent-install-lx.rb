appd = node['ihg-appdagent']

%W(#{appd.install_dir} #{appd.machineagent.install_dir} #{appd.appagent_dir}).each do |dir|
  directory dir do
    user   appd.machineagent.user
    group  appd.machineagent.group
    recursive true
    mode   '0755'
    action :create
  end
end

package 'Install unzip' do
  case node['platform']
  when 'redhat', 'centos'
    package_name 'unzip'
  end
  not_if { File.exist?("#{appd.machineagent.install_dir}/machineagent.jar") }
end

package 'Install nc' do
  case node['platform']
  when 'redhat', 'centos'
    package_name 'nc'
  end
  not_if { File.exist?('/usr/bin/nc') }
end

remote_file "/tmp/#{appd.machineagent.pkg}" do
  source   appd.machineagent.pkg_url
  user     appd.machineagent.user
  group    appd.machineagent.group
  mode     '0644'
  backup   false
  not_if { File.exist?("#{appd.machineagent.install_dir}/machineagent.jar") }
end

execute 'unzip_machine_agent' do
  command <<-EOH
    unzip -q /tmp/#{appd.machineagent.pkg} -d #{appd.machineagent.install_dir}
    chown -R #{appd.machineagent.user}:#{appd.machineagent.group} #{appd.machineagent.install_dir}
    rm #{"/tmp/#{appd.machineagent.pkg}"}
  EOH
  action :run
  not_if { File.exist?("#{appd.machineagent.install_dir}/machineagent.jar") }
end

link "#{appd.install_dir}/machine_agent/latest" do
  to appd.machineagent.install_dir
end

# execute 'Remove Previous Machine Agent Version' do
#   command <<-EOH
#     find #{appd.install_dir}/machine_agent -maxdepth 1 -mindepth 1 -type d \
#     | grep -v #{appd.machineagent.version} | xargs rm -rf
#   EOH
#   action :run
# end

cookbook_file "#{appd.machineagent.install_dir}/appdynamics-init-machine-agent" do
  source 'appdynamics-machine-agent'
  user     appd.machineagent.user
  group    appd.machineagent.group
  mode     '0755'
end

link '/etc/init.d/appd_agent' do
  to "#{appd.install_dir}/machine_agent/latest/appdynamics-init-machine-agent"
end

link '/local/java/appd_agent' do
  to "/local/java/jdk1.8.0_191"
end

template "#{appd.machineagent.install_dir}/appdynamics-sysconfig-machine-agent" do
  source 'machine-agent-sysconfig-lx.erb'
  user     appd.machineagent.user
  group    appd.machineagent.group
  mode     '0644'
  variables(
    lazy do
      {
        appd_user: appd.machineagent.user, appd_group: appd.machineagent.group,
        contr_host: appd.controller.host, acc_name: appd.controller.acc_name,
        acc_key: appd.controller.acc_key, appd_install_dir: "#{appd.install_dir}/machine_agent/latest",
        hierarchy_path: node['ihg-appdagent']['machineagent']['hierarchy_path']
      }
    end
  )
  notifies :restart, 'service[appd_agent]', :delayed
end

link '/etc/sysconfig/appdynamics-machine-agent' do
  to "#{appd.install_dir}/machine_agent/latest/appdynamics-sysconfig-machine-agent"
end

# Log4j settings
template "#{appd.machineagent.install_dir}/conf/logging/log4j.xml" do
  source 'machine_agent_log4j.erb'
  user    appd.machineagent.user
  group   appd.machineagent.group
  only_if { ::Dir.exist?("#{appd.machineagent.install_dir}/") }
end

ruby_block 'Get Monitoring Templates From CMDB' do
  block do
    Chef::Resource::RubyBlock.send(:include, Chef::Mixin::ShellOut)
    url = 'https://dash.ihg.com/tools/universal_column_edit.php' \
          '?db=cmdb&table=cmdb&uid=id&columns=monitoring_template&api=json' \
          "&where=ci_name%3D%27#{node['fqdn']}%27"
    Chef::Log.info("MONITORING TEMPLATE LIST URL: #{url}")
    command_out = shell_out("curl '#{url}'")
    response = JSON.parse(command_out.stdout)
    next if response.nil? || response.count < 1
    templates = response[0]['monitoring_template'].split(',').sort()
    Chef::Log.info("MONITORING TEMPLATES: #{templates.join(',')}")
    node.default['ihg-appdagent']['monitoring_template'] = templates
  end
  action :nothing
  ignore_failure true
end.run_action(:run)

ruby_block 'Generate Hierarchy Path' do
  block do
    workgroups = []
    functions = []
    node['ihg-appdagent']['monitoring_template'].each do |template|
      pieces = template.split('-')
      workgroups.push(pieces[0]) unless pieces[0].nil?
      functions.push([pieces[1]]) unless pieces[1].nil?
    end
    path = "#{node['ihg-appdagent']['datacenter']}|" \
           "#{node.chef_environment}|" \
           "#{node['os']}|" \
           "#{workgroups.uniq.sort.join(',')}|" \
           "#{functions.uniq.sort.join(',')}|".downcase

    Chef::Log.info("GENERATED HIERARCHY PATH: #{path}")
    if path.length > 95
      Chef::Log.warn("!!! HIERARCHY PATH LENGTH OF #{path.length} EXCEEDS LIMIT OF 95 !!!")
      Chef::Log.warn("REVERTING TO: #{appd.machineagent.hierarchy_path}")
      next
    end
    node.default['ihg-appdagent']['machineagent']['hierarchy_path'] = path
  end
  action :nothing
  not_if { node['ihg-appdagent']['monitoring_template'].empty? }
end.run_action(:run)

ruby_block 'Get app info from tier-config file' do
  block do
    file = nil
    if ::File.exist?("#{appd.machineagent.install_dir}/tier-config")
      file = ::File.read("#{appd.machineagent.install_dir}/tier-config")
    elsif ::File.exist?("#{appd.machineagent.install_dir}/../tier-config")
      file = ::File.read("#{appd.machineagent.install_dir}/../tier-config")
    end
    if file
      if (match = file.match(/TIER_NAME=(.*?)$/))
        node.default['ihg-appdagent']['tier_name'] = match[1]
      end
    end
  end
  action :nothing
end.run_action(:run)

ruby_block 'Get App Monitors From Dash' do
  block do
    # tricky way to load this Chef::Mixin::ShellOut utilities
    Chef::Resource::RubyBlock.send(:include, Chef::Mixin::ShellOut)
    url = 'https://dash.ihg.com/tools/universal_column_edit.php?' \
          'db=monitors&table=monitor_template&uid=id&columns=*&api=json&where='
    node['ihg-appdagent']['monitoring_template'].each do |template|
      wid, fn = template.split('-')
      url += "%28workgroup=%27#{wid}%27AND%20function=%27#{fn}%27%29OR"
    end
    url.chomp!('OR')
    Chef::Log.info("APP MONITORS URL: #{url}")
    command = "curl '#{url}'"
    command_out = shell_out(command)
    response = JSON.parse(command_out.stdout)
    if !response.nil? && response.count > 0
      logmonitors = Hash.new { |h, k| h[k] = [] } # Hash of array
      processmonitors = []
      urlmonitors = []
      wmqmonitors = Hash.new { |h, k| h[k] = [] } # Hash of array
      cwmonitors = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) } # auto-vivifying
      jmxmonitors = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) } # auto-vivifying
      jmx = { port: 9444, user: 'jmxmon' }
      response.each do |monitor|
        tierenv = {
          'a' => 'Acceptance', 'd' => 'Development', 'i' => 'Integration', 'e' => 'Performance',
          'r' => 'Perftest', 'p' => 'Production', 'q' => 'QA', 't' => 'QAP', 's' => 'Staging', 'u' => 'UTC'
        }.fetch(monitor['env'], '')
        next if (monitor['generated_label'].empty? && monitor['health_rule_name'].empty?) ||
                (tierenv != node.chef_environment && tierenv != '')
        monitor['dc'] = 'adc' if monitor['dc'] == 'atld1'
        next unless monitor['dc'].empty? || monitor['dc'].include?(appd.datacenter.downcase)

        case monitor['typeName'].upcase
        when /^PROCESS/
          if monitor['location'].empty?
            monitor['location'] = "-Dihg.app.id=#{monitor['workgroup']}#{monitor['function']}"
          end
          processmonitors.push(monitor['location'])
        when /^LOG/
          monitor['regex'] = '(?<!On)OutOfMemoryError' if monitor['regex'] == 'OutOfMemoryError'
          logmonitors[monitor['location']].push(monitor['regex'])
        when /^NETSTAT/
          name = "Netstat|#{monitor['location']}_#{monitor['attribute']}_#{monitor['regex']}"
          cwmonitors[name]['port'] = monitor['location']
          cwmonitors[name]['protocol'] = monitor['attribute']
          cwmonitors[name]['state'] = monitor['regex']
        when /^PORT/
          name = "Port|#{monitor['location']}_#{monitor['attribute']}"
          if monitor['location'].empty?
            monitor['location'] = 'localhost'
            name = "Port|#{monitor['attribute']}"
          end
          cwmonitors[name]['port'] = monitor['attribute']
          cwmonitors[name]['host'] = monitor['location']
        when /^JMX/
          next if monitor['monitor_type'] == '9' # Active Threads
          jmx[:port] = monitor['attribute2'] if !monitor['attribute2'].empty? && monitor['attribute2'] =~ /^\d{4}/
          jmx[:port] = monitor['location'] if !monitor['location'].empty? && monitor['location'] =~ /^\d{4}/
          jmx[:user] = monitor['user'] unless monitor['user'].empty?
          jmx['serviceUrl'] = "service:jmx:rmi:///jndi/rmi://localhost:#{jmx[:port]}/" \
              + (!monitor['attribute3'].empty? ? monitor['attribute3'] : 'jmxrmi')
          if monitor['monitor_type'] == '2' # Heap
            jmxmonitors['heap'] = monitor['attribute2']
            next
          end
          jmxmonitors['mbeans'][monitor['location']][monitor['attribute']] = { regex: monitor['regex'] }
        when 'WEB CHECK'
          urlmon = {
            'label' => monitor['label'],
            'user' => monitor['user'],
            'pass' => monitor['pass'],
            'port' => monitor['attribute2'],
            'payload' => monitor['attribute3'],
            'hostname' => !monitor['attribute4'].empty? ? monitor['attribute4'] : 'localhost',
            'ssl' => monitor['attribute5'],
            'path' => monitor['location'],
            'timeout' => monitor['attribute'],
            'regex' => monitor['regex'],
            'headers' => '',
            'payload_file' => '',
          }
          begin
            urlmon['headers'] = JSON.parse(monitor['attribute6']) unless monitor['attribute6'].empty?
          rescue => e
            puts e.inspect
          end
          unless urlmon['payload'].empty?
            urlmon['payload_file'] = 'payload_' + Digest::MD5.hexdigest(urlmon['payload']) + '.json'
          end
          urlmon['url'] = 'http' + (urlmon['ssl'] == 'True' ? 's' : '') + "://#{urlmon['hostname']}" \
              + (!urlmon['port'].empty? ? ":#{urlmon['port']}" : '') + urlmon['path']
          urlmonitors.push(urlmon)
        when 'IBM MQ CHECK'
          wmqmonitors[monitor['attribute2']].push(monitor['attribute3']) unless \
            wmqmonitors[monitor['attribute2']].include?(monitor['attribute3'])
          name = "WebsphereMQCustom|#{monitor['attribute2']}|Queues|#{monitor['attribute3']}|#{monitor['attribute']}"
          # Using CommandWatcher extension for now since WMQ Monitor doesn't work reliably
          cwmonitors[name]['manager'] = monitor['attribute2']
          cwmonitors[name]['queue'] = monitor['attribute3']
          cwmonitors[name]['metric'] = monitor['attribute']
        end
      end
      logmonitors.each { |location, _regex| logmonitors[location].uniq! }
      node.default['ihg-appdagent']['machineagent']['extensions']['LogMonitor']['app_monitors'] = logmonitors
      node.default['ihg-appdagent']['machineagent']['extensions']['ProcessMonitor']['app_monitors'] = processmonitors
      node.default['ihg-appdagent']['machineagent']['extensions']['UrlMonitor']['app_monitors'] = urlmonitors
      node.default['ihg-appdagent']['machineagent']['extensions']['CommandWatcher']['app_monitors'] = cwmonitors
      node.default['ihg-appdagent']['machineagent']['extensions']['WMQMonitor']['app_monitors'] = wmqmonitors
      # If there aren't any monitors for the WID-function, make sure nothing gets added to the monitors hash.
      # This will ensure that the extension does not get installed if not necessary.
      unless jmxmonitors.empty?
        node.default['ihg-appdagent']['machineagent']['extensions']['JMXMonitor']['app_monitors'] = {
          connection: jmx, mons: jmxmonitors
        }
      end
    end
  end
  action :nothing
  not_if { appd.workgroup == 'tst' || node['ihg-appdagent']['monitoring_template'].empty? }
  ignore_failure true unless node.attribute?('kitchen')
end.run_action(:run)

# Install and configure extensions
node['ihg-appdagent']['machineagent']['extensions'].each do |name, ext|
  if %w(Production UTC Training).include?(node.chef_environment) &&
     !%w(LogMonitor ProcessMonitor JMXMonitor CommandWatcher UrlMonitor WMQMonitor ApigeeMonitor).include?(name)
    puts "#{name}|#{ext}: Skipped due to Environment and Name constraints"
    next
  end
  extnpath = "#{appd.machineagent.install_dir}/monitors/#{name}"
  confpath = ext.conf_dir.empty? ? extnpath : "#{extnpath}/#{ext.conf_dir}"
  ext_version = 0
  if (match = ext['pkg'].match(/((?:\d+\.?)+)\.zip$/))
    ext_version = match[1]
  end

  # Create the extension directory
  directory extnpath do
    user   appd.machineagent.user
    group  appd.machineagent.group
    recursive true
    mode   '0755'
    action :nothing
  end

  # Check to see if extension is needed
  if !ext['default'] && ext['app_monitors'].empty?
    ruby_block "remove_unused_extension_#{name}" do
      block do
      end
      action :run
      notifies :delete, "directory[#{extnpath}]", :immediately
      notifies :restart, 'service[appd_agent]', :delayed
      only_if { ::Dir.exist?(extnpath) }
    end
    node.rm('ihg-appdagent', 'machineagent', 'extensions', name)
    next
  end
  puts "\n*** #{name} ***\n" + Chef::JSONCompat.to_json_pretty(ext) if node.attribute?('kitchen')

  # Get the remote file
  remote_file "/tmp/#{ext.pkg}" do
    source "#{appd.repo_url}/extensions/#{ext.pkg}"
    user   appd.machineagent.user
    group  appd.machineagent.group
    mode   '0444'
    action :create
    not_if { File.exist?("#{extnpath}/extension_version") && open("#{extnpath}/extension_version").read.include?(ext_version) }
  end

  # Unzip the extension file
  execute "unzip_#{name}" do
    command <<-EOH
    unzip -q /tmp/#{ext.pkg} -d #{appd.machineagent.install_dir}/monitors/
    chown -R #{appd.machineagent.user}:#{appd.machineagent.group} #{extnpath}
    rm /tmp/#{ext.pkg}
    EOH
    action :nothing
    subscribes :run, "remote_file[/tmp/#{ext.pkg}]", :immediately
    notifies :delete, "directory[#{extnpath}]", :before
    notifies :create, "directory[#{extnpath}]", :before
  end

  file "create_#{name}_version_file" do
    path "#{extnpath}/extension_version"
    content "#{ext_version}\n"
    owner appd.machineagent.user
    group appd.machineagent.group
    mode '0755'
    action :nothing
    subscribes :create, "execute[unzip_#{name}]", :immediately
  end

  # Get the prerequisite of jq version which has to be installed
  if appd.workgroup == 'a05' && ext.default_conf == 'config.json' && name == 'ApigeeMonitor'
    remote_file "/usr/bin/jq" do
      source "https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64"
      user   'root'
      group  'root'
      mode   '0755'
      action :create
    end
  end

  # Create payload files for the URL extension
  if name == 'UrlMonitor'
    mons_with_payload = ext.app_monitors.select { |e| !e.payload_file.empty? }.uniq(&:payload_file)
    puts "\n*** URLMONS WITH PAYLOAD ***\n" + Chef::JSONCompat.to_json_pretty(mons_with_payload) if node.attribute?('kitchen')
    mons_with_payload.each do |mon|
      file "#{name}_#{mon.payload_file}" do
        name "#{extnpath}/#{mon.payload_file}"
        content mon.payload
        user appd.machineagent.user
        group appd.machineagent.group
        mode '0755'
        action :create
      end
    end
  end

  # Configure extension conf for ApigeeMonitor & Others
  if appd.workgroup == 'a05' && ext.default_conf == 'config.json' && name == 'ApigeeMonitor'
    template "#{confpath}/#{ext.default_conf}" do
      source "#{name}_config.json.erb"
      user     appd.machineagent.user
      group    appd.machineagent.group
      variables(monitors: ext.app_monitors)
      only_if { ::Dir.exist?(confpath) }
      not_if { File.exist?("#{confpath}/disable_conf_overwrite") } # For testing, to prevent config changes
    end
    service 'appd_agent' do
      action :restart
      ignore_failure true
    end
  else
    template "#{confpath}/#{ext.default_conf}" do
      source "#{name}_config.yml.erb"
      user     appd.machineagent.user
      group    appd.machineagent.group
      variables(monitors: ext.app_monitors)
      only_if { ::Dir.exist?(confpath) }
      not_if { File.exist?("#{confpath}/disable_conf_overwrite") } # For testing, to prevent config changes
    end
    template "#{extnpath}/monitor.xml" do
      source "#{name}_monitor.xml.erb"
      user     appd.machineagent.user
      group    appd.machineagent.group
      mode     '0644'
      variables(state: 'true')
      notifies :restart, 'service[appd_agent]', :delayed
      only_if { File.exist?("#{confpath}/#{ext.default_conf}") }
    end
  end
end

# execute 'copy_apache_extension' do
#   command <<-EOH
#   cp -rp /local/apps/appdynamics/machineagent/monitors/ApacheMonitor #{appd.machineagent.install_dir}/monitors/
#   chown -R #{appd.machineagent.user}:#{appd.machineagent.group} #{appd.machineagent.install_dir}/monitors/ApacheMonitor
#   EOH
#   only_if { ::Dir.exist?('/local/apps/appdynamics/machineagent/monitors/ApacheMonitor') }
#   not_if { ::Dir.exist?("#{appd.machineagent.install_dir}/monitors/ApacheMonitor") }
# end

service 'appd_machine' do
  action :nothing
  ignore_failure true
end

if ::Dir.exist?('/local/apps/appdynamics/machineagent') || ::Dir.exist?('/local/appdynamics/machineagent')
  file 'appd_machine_file' do
    action :delete
    name '/etc/init.d/appd_machine'
    notifies :stop, 'service[appd_machine]', :before
    notifies :disable, 'service[appd_machine]', :before
  end

  execute 'kill_old_machineagent' do
    command <<-EOH
      PID=$(ps -ef | grep ".* java -jar /apps/appdynamics/machineagent/machineagent.jar" | grep -v grep | awk '{print $2}')
      [ -z "$PID" ] || xargs kill "$PID" 2>&1 >/dev/null
    EOH
    action :run
  end
end

execute 'ensure_machine_agent_is running' do
  command '/etc/init.d/appd_agent status || /etc/init.d/appd_agent restart'
  ignore_failure true
end

service 'appd_agent' do
  supports status: true, restart: true, reload: false
  action [:start, :enable]
end

# Copy the modified init file
# Create link from the init scripts
# Copy the Sysconfig template
# Start the agent
