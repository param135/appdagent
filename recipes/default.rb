# Cookbook Name:: ihg-appdagent
# Recipe:: default
#
# Copyright 2018, IHG
# All rights reserved - Do Not Redistribute

case node['platform_family']
when 'rhel'
  appd = node['ihg-appdagent']
  BUSINESS_GROUPS = %w(SYS CORP).freeze
  ENVIRONMENTS = %w(Integration QAP QA Staging Perftest Performance UTC Training Production).freeze
  APM_ENVIRONMENTS = %w(Development Integration QAP QA Staging Perftest).freeze
  if (
    (
      (node.chef_environment == 'Development' && node['Zone'] == 'Public' && BUSINESS_GROUPS.include?(node['Business_Group'])) ||
      (APM_ENVIRONMENTS.include?(node.chef_environment) && ::Dir.exist?('/local/apps/appdynamics/machineagent')) ||
      ENVIRONMENTS.include?(node.chef_environment)
    ) && appd.workgroup != 'tst' && appd.datacenter != 'MKSD1'
  ) || appd.datacenter == 'MKSD1' && %w(bie apd).include?(appd.workgroup)
    include_recipe 'ihg-appdagent::machine-agent-install-lx'
  else
    service 'appd_agent' do
      action [:stop, :disable]
    end
    node.default['ihg-appdagent'] = false
  end
when 'windows'
  case node['ihg-appdagent']['datacenter']
  when 'IADD1', 'SJCD1', 'MKSD1', 'ADC', 'RDUD1', 'WBUD1', 'SHAD1', 'ATLR3', 'PVGD1', 'DCAD1'
    include_recipe 'ihg-appdagent::version'
    include_recipe 'ihg-appdagent::machine-agent-install-win'
    include_recipe 'ihg-appdagent::deploy_monitors'
  end
end
