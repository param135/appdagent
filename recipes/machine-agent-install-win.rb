# Windows Server only
case node['platform_family']
when 'windows'
  appd = node['ihg-appdagent']
  if appd.workgroup == 'tst'
    dsc_resource 'Stop Appdynamics Service' do
      resource :Service
      module_name 'PSDesiredStateConfiguration'
      property :Name, 'Appdynamics Machine Agent'
      property :Ensure, 'Absent'
      property :State, 'Stopped'
    end
    return
  end

  # Below windows_zipfile resource is depending on rubyzip to unzip the agent package
  # which is not working due to compatabiltiy with ruby version requires >=2.4 version.
  # Refactoring the below windows_zipfile resource with powershell_script resource to extract the appd machine agent archieve in windows server.

  # windows_zipfile node['ihg-appdagent']['install_dir'] do
  #   source node['ihg-appdagent']['machineagent']['pkg_url']
  #   action :unzip
  #   not_if { ::Dir.exist?(node['ihg-appdagent']['install_dir']) }
  #   notifies :run, 'powershell_script[Restart Appdynamics Service]', :immediately
  # end
  
  directory node['ihg-appdagent']['install_dir'] do
    action :create
    not_if { ::Dir.exist?(node['ihg-appdagent']['install_dir']) }
  end

  remote_file node['ihg-appdagent']['machineagent']['zip_path'] do
    source node['ihg-appdagent']['machineagent']['pkg_url']
    checksum node['ihg-appdagent']['machineagent']['checksum']
    action :create
    not_if { ::File.exist?(node['ihg-appdagent']['machineagent']['zip_path']) }
    notifies :run, 'powershell_script[Expand Appdynamics Package]', :immediately
  end

  powershell_script 'Expand Appdynamics Package' do
    code <<-EOH
    Expand-Archive -LiteralPath #{node['ihg-appdagent']['machineagent']['zip_path']} -DestinationPath #{node['ihg-appdagent']['install_dir']}
    EOH
    action :nothing
    guard_interpreter :powershell_script
    only_if { ::File.exist?(node['ihg-appdagent']['machineagent']['zip_path']) }
  end

  template "#{node['ihg-appdagent']['install_dir']}\\InstallService.vbs" do
    source 'InstallService.vbs.erb'
    action :create
    notifies :run, 'powershell_script[Restart Appdynamics  Service]', :immediately
  end

  template "#{node['ihg-appdagent']['install_dir']}\\conf\\controller-info.xml" do
    source 'controller-info_xml.erb'
    action :create
    notifies :run, 'powershell_script[Restart Appdynamics  Service]', :immediately
  end

  execute 'Install Appdynamics Service' do
    command "cscript #{node['ihg-appdagent']['install_dir']}\\InstallService.vbs"
    guard_interpreter :powershell_script
    not_if "Get-Service 'Appdynamics Machine Agent'"
  end

  dsc_resource 'Start Appdynamics  Service' do
    resource :Service
    module_name 'PSDesiredStateConfiguration'
    property :Name, 'Appdynamics Machine Agent'
    property :Ensure, 'Present'
    property :State, 'Running'
  end

  powershell_script 'Restart Appdynamics  Service' do
    code <<-EOH
    Restart-Service -Name 'Appdynamics Machine Agent'
    EOH
    action :nothing
    guard_interpreter :powershell_script
    only_if "Get-Service -Name 'Appdynamics Machine Agent'"
  end
end
