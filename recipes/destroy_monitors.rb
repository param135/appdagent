#
# Cookbook:: ihg-appdagent
# Recipe:: destroy_monitors
#
# Copyright:: 2018, The Authors, All Rights Reserved.

case node['platform_family']
when 'windows'
  dsc_resource 'Stop Appdynamics Service' do
    resource :Service
    module_name 'PSDesiredStateConfiguration'
    property :Name, 'Appdynamics Machine Agent'
    property :Ensure, 'Present'
    property :State, 'Stopped'
    guard_interpreter :powershell_script
    only_if "Get-Service -Name 'Appdynamics Machine Agent'"
  end

  directory "#{node['ihg-appdagent']['monitor_dir']}\\" + node['ihg-appdagent']['custom_extension'].Name do
    action :delete
    recursive true
  end

  dsc_resource 'Start Appdynamics Service' do
    resource :Service
    module_name 'PSDesiredStateConfiguration'
    property :Name, 'Appdynamics Machine Agent'
    property :Ensure, 'Present'
    property :State, 'Running'
    guard_interpreter :powershell_script
    only_if "Get-Service -Name 'Appdynamics Machine Agent'"
  end
end

# Remove zip file
file "#{Chef::Config[:file_cache_path]}\\" + node['ihg-appdagent']['custom_extension'].ZipFile do
  action :delete
end
