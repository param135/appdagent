#
# Cookbook:: ihg-appdagent
# Recipe:: destroy_agent
#
# Copyright:: 2019, The Authors, All Rights Reserved.

install_dir = node['ihg-appdagent']['install_dir']

case node['platform_family']
when 'windows'
  dsc_resource 'Stop Appdynamics Service' do
    resource :Service
    module_name 'PSDesiredStateConfiguration'
    property :Name, 'Appdynamics Machine Agent'
    property :Ensure, 'Present'
    property :State, 'Stopped'
    guard_interpreter :powershell_script
    only_if "Get-Service -Name 'Appdynamics Machine Agent'"
  end
end

execute 'Uninstall Appdynamics Service' do
  command "cscript #{node['ihg-appdagent']['install_dir']}\\UninstallService.vbs"
  guard_interpreter :powershell_script
  only_if "Get-Service 'Appdynamics Machine Agent'"
end

powershell_script 'Remove Appdynamics Directory' do
  code <<-ENDCOMMAND
  Remove-Item #{install_dir} -Force -Recurse
  Remove-Item "#{Chef::Config[:file_cache_path]}\\machineagent-bundle*.zip"
  ENDCOMMAND
  only_if { File.exist?(install_dir) }
end
