#
# Cookbook:: ihg-appdagent
# Recipe:: deploy_monitors
#
# Copyright:: 2018, The Authors, All Rights Reserved.

case node['platform_family']
when 'windows'
  # Below windows_zipfile resource is depending on rubyzip to unzip the agent package
  # which is not working due to compatabiltiy with ruby version requires >=2.4 version.
  # Refactoring the below windows_zipfile resource with powershell_script resource to extract the appd machine agent monitor archieve in windows server.

  # windows_zipfile node['ihg-appdagent']['monitor_dir'] do
  #   source "#{node['ihg-appdagent']['repo_url']}/extensions/" + node['ihg-appdagent']['custom_extension'].ZipFile
  #   action :unzip
  #   not_if { ::File.exist?("#{Chef::Config[:file_cache_path]}\\" + node['ihg-appdagent']['custom_extension'].ZipFile) }
  #   notifies :run, 'powershell_script[Restart Appdynamics Service]', :immediately
  # end

  directory node['ihg-appdagent']['monitor_dir'] do
    action :create
    not_if { ::Dir.exist?(node['ihg-appdagent']['monitor_dir']) }
  end

  remote_file "#{node['ihg-appdagent']['monitor_dir']}/#{node['ihg-appdagent']['custom_extension'].ZipFile}" do
    source "#{node['ihg-appdagent']['repo_url']}/extensions/#{node['ihg-appdagent']['custom_extension'].ZipFile}"
    action :create
    not_if { ::File.exist?("#{node['ihg-appdagent']['monitor_dir']}/#{node['ihg-appdagent']['custom_extension'].ZipFile}") }
    notifies :run, 'powershell_script[Expand Appdynamics monitor Package]', :immediately
  end

  powershell_script 'Expand Appdynamics monitor Package' do
    code <<-EOH
    Expand-Archive -LiteralPath "#{node['ihg-appdagent']['monitor_dir']}/#{node['ihg-appdagent']['custom_extension'].ZipFile}" -DestinationPath #{node['ihg-appdagent']['monitor_dir']}
    EOH
    action :nothing
    guard_interpreter :powershell_script
    only_if { ::File.exist?("#{node['ihg-appdagent']['monitor_dir']}/#{node['ihg-appdagent']['custom_extension'].ZipFile}") }
    notifies :run, 'powershell_script[Restart Appdynamics Service]', :immediately
  end

  powershell_script 'Restart Appdynamics Service' do
    code <<-EOH
    Restart-Service -Name 'Appdynamics Machine Agent'
    EOH
    action :nothing
    guard_interpreter :powershell_script
    only_if "Get-Service -Name 'Appdynamics Machine Agent'"
  end
end
