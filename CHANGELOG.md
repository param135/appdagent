# ihg-appdAgent CHANGELOG

## 1.0.30 - Sep 07, 2021  
- [Harish Bandari] - Replaced windows_zip to powershell_script to fix rubyzip issue

## 1.0.29 - Jun 08, 2021  
- [Parameshwara] - Attribute change for equery_interval_in_secs from 90 to 1800

## 1.0.28 - May 21, 2021  
- [Parameshwara] - Apigee Monitor Extension Upgrade to version 21.1.3

## 1.0.27 - Apr 13, 2021  
- [Parameshwara] - New Extension 21.1.1 expanded for Prod

## 1.0.26 - Apr 12, 2021  
- [Parameshwara] - Post handling the maximum foodcritic exception

## 1.0.25 - Apr 12, 2021  
- [Parameshwara] - Post Handling the foodcritic exception

## 1.0.24 - Apr 12, 2021  
- [Parameshwara] - Extension repository changed to 21.1.1 Only On Staging

## 1.0.23 - Sep 10, 2020  
- [Parameshwara] - Recreate the production CMR

## 1.0.22 - Sep 4, 2020  
- [Parameshwara] - Replaced .delivery directory and bumped the version

## 1.0.21 - Sep 3, 2020  
- [Parameshwara] - Post handling the restart of the appd service and review bump

## 1.0.20 - Sep 3, 2020  
- [Parameshwara] - Redirecting a better version of the Artifactory with the apiproxy.conf

## 1.0.19 - Sep 2, 2020  
- [Parameshwara] - Added further validations & Handled restart of the service

## 1.0.18 - Sep 1, 2020  
- [Parameshwara] - Post changes for Appdynamics Apigee Cloud Monitor Extension

## 1.0.17 - July 17, 2020  
- [Birendra Singh] - Commit issue- retrying

## 1.0.16 - July 16, 2020  
- [Birendra Singh] - Fix EL7 systemd startup; Execute bit for appd_agent -> appdynamics-init-machine-agent

## 1.0.13 - April 23rd 2020
- [Dave Pechous] - DE51628 - added install of 'nc' command on linux

## 1.0.11 - April 8th 2020
- [Karthik Jangiti] - Increase MAX_METRIC limit to 25K
## 1.0.10 - August 23rd 2019
- [Karthik Jangiti] - US85596 Appdynamics machine agent :: adding monitor apigee

## 1.0.9 - August 22nd 2019
- [Karthik Jangiti] - US85596 Appdynamics machine agent :: adding monitor apigee

## 1.0.8 - July 11th 2019
- [Param] - US84123 Application Dynamics Port Check enhancement

## 1.0.7 - July 10th 2019
- [Saurabh Bhatia(HCL)] - US80975 Remove Google Update Service from Windows Boxes
## 1.0.6 - June 27th 2019
- [Param] - US78317 Post fixing the blank hostname from the previous release
## 1.0.5 - June 21st 2019
- [Param] - US78317 Post Conflict resolution
## 1.0.4 - June 20th 2019
- [Param] - US78317 Suppressed the false alarm by fixing the Port Check

## 1.0.3 - June 19th 2019
- Adding AWS to list of datacenters

## 1.0.2 - May 24th 2019
- [Param] - US78317 Correction emphasized done for netcat utility
## 1.0.1 - May 23rd 2019
- [Param] - US78317 Correction done for netcat utility
## 1.0.0 - May 21st 2019
- [Derrick Shultz] - Add more metrics to WMQ monitor
## 0.7.1 - May 9th 2019
## 0.7.0 - May 2nd 2019
- [Dave Pechous] - US64515 Update Appd: Write update and destroy recipes for Windows
                 - https://rally1.rallydev.com/#/229113251508d/detail/userstory/274092412480
                 - Added destroy_all recipe.
                 - Updated attributes to new version 4.5.10-2131.
                 - Added destroy_all run to version.rb.
                 - Added remove of zip file to destroy_monitors.
## 0.6.40 - April 12th 2019
- [Derrick Shultz] - Look for tier-config in parent directory
## 0.6.39 - April 9th 2019
- [Derrick Shultz] - Increase MAX_METRIC limit per AppD suppport. Fixes missing metrics
## 0.6.38 - April 3rd 2019
- [Derrick Shultz] - Dedupe WMQ queue names and fix monitor environment checking
## 0.6.37 - April 2nd 2019
- [Derrick Shultz] - Adjust JMX object path parser
## 0.6.36 - March 4th 2019
- [Derrick Shultz] - Monitor bluestripe on certain WIDs
## 0.6.35 - February 13th 2019
- [Scott Cochran] - remove redundancy in windows attributes
## 0.6.34 - February 8th 2019
- [Scott Cochran] - allow adiap hosts in mkds1 to connect to appdynamics through the proxy
## 0.6.33 - January 22nd 2019
- [Derrick Shultz] - Set attribute to false when agent not used
## 0.6.32 - January 18th 2019
- [Derrick Shultz] - Fix chef-client log error matching
## 0.6.31 - January 18th 2019
- [Scott Cochran] - Update port check to try both commands instead of just one
## 0.6.30 - January 17th 2019
- [Scott Cochran] - update to use ncat if /dev/tcp does not exist
## 0.6.29 - January 11th 2019
- [Derrick Shultz] - Update monitor type matching
## 0.6.28 - January 10th 2019
- [Scott Cochran] - Update WMQMonitor config for bug fixes.  Update WMQCustom with new perl command
## 0.6.27 - January 9th 2019
- [Scott Cochran] - Remove extra backslash from command
## 0.6.26 - January 9th 2019
- [Scott Cochran] - Create custom WMQ Monitor using local MQ commands
## 0.6.25 - January 4th 2019
- [Scott Cochran] - Update WMQMonitor extension to version 7.0.1
## 0.6.24 - January 4th 2019
- [Derrick Shultz] - Update agent
## 0.6.23 - January 2nd 2019
- [Derrick Shultz] - Change Berkfile order
## 0.6.22 - January 2nd 2019
- [Derrick Shultz] - Remove ipremoted process check
## 0.6.21 - December 21st 2018
- [Scott Cochran] - Bumping version to push through delivery
## 0.6.20 - December 21st 2018
- [Scott Cochran] - Add WMQMonitors to Production list
## 0.6.19 - December 21st 2018
- [Derrick Shultz] - Use new CMDB
## 0.6.18 - December 20th 2018
- [Derrick Shultz] - Fixed environment matching for monitors
## 0.6.17 - December 20th 2018
- [Scott Cochran] - Fixed odd characters in WMQ config yaml
## 0.6.16 - December 19th 2018
- [Jason Roth] - Add managed datacenters
## 0.6.15 - December 18th 2018
- [Derrick Shultz] - Override hierarchy path for aqm servers
## 0.6.14 - December 17th 2018
- [Derrick Shultz] - Disable agent on environment downgrade
## 0.6.13 - December 7th 2018
- [Derrick Shultz] - Downgrade agent to fix memory calculation issue
## 0.6.12 - December 7th 2018
- [Derrick Shultz] - Use monitoring_template for hierarchy path and monitors instead of base wid-fn
## 0.6.11 - December 7th 2018
- [Scott Cochran] - remove items from controller info that should be for app agent only
## 0.6.10 - December 7th 2018
- [Scott Cochran] - update app and machines agents on google cloud hosts to point to correct proxy host/port
## 0.6.9 - December 6th 2018
- [Derrick Shultz] - Fix headers with JSON
## 0.6.8 - December 6th 2018
- [Derrick Shultz] - Dedupe process and log monitors
## 0.6.7 - December 4th 2018
- [Derrick Shultz] - Fix init script case statement default
## 0.6.6 - November 30th 2018
- [Derrick Shultz] - Fixed JMX port missing from some connection strings
## 0.6.5 - November 12th 2018
- [Derrick Shultz] - Stop old machine agent
## 0.6.4 - November 12th 2018
- [Derrick Shultz] - Promote -Xms512 to prod
## 0.6.3 - November 12th 2018
- [Derrick Shultz] - No more disable_agent feature
## 0.6.2 - November 12th 2018
- [Derrick Shultz] - Remove more cases of tst workgroup
## 0.6.0 - November 9th 2018
- [Scott Cochran] - Add proxy configuration for mksd1 servers
## 0.6.0 - November 9th 2018
- [Derrick Shultz] - Update Linux machine agent to 4.5.4 in non-prod
## 0.5.22 - October 29th 2018
- [Jason Roth] - Add ATLR3 datacenter
## 0.5.21 - October 29th 2018
- [Derrick Shultz] - Prevent agent from restarting on every run
## 0.5.20 - October 17th 2018
- [Derrick Shultz] - Update LogMonitor to 3.0.1
## 0.5.19 - October 16th 2018
- [Derrick Shultz] - Re-push through automate
## 0.5.18 - October 15th 2018
- [Derrick Shultz] - Update min heap to match max
## 0.5.17 - October 11th 2018
- [Derrick Shultz] - Fix Log Monitor config
## 0.5.16 - October 10th 2018
- [Derrick Shultz] - Create extension version files to allow updates
## 0.5.15 - October 9th 2018
- [Derrick Shultz] - Update Log Monitor extension
## 0.5.14 - October 9th 2018
- [Scott Cochran] - stop monitoring bluestripe process if disable attribute is true
## 0.5.13 - September 25th 2018
- [Derrick Shultz] - Update OutOfMemoryError regex and Log Monitor threads
## 0.5.12 - September 25th 2018
- [Scott Cochran] - Add WMQMonitor extension for IBM MQ Checks
## 0.5.11 - September 19th 2018
- [Jason Roth] - Updated attributes in Windows recipes to reflect new names
## 0.5.10 - September 18th 2018
- [Scott Cochran] - Add attribute for appd agent enabled/disabled
## 0.5.9 - September 18th 2018
- [Derrick Shultz] - Fix JMX config for other types and users
## 0.5.8 - September 17th 2018
- [Scott Cochran] - Add payloads to web checks
## 0.5.7 - September 14th 2018
- [Derrick Shultz] - Add int controller
## 0.5.6 - September 13th 2018
- [Derrick Shultz] - Promote URL extension to prod, UTC and training
## 0.5.5 - September 10th 2018
- [Scott Cochran] - Include CommandWatcher module in Production
## 0.5.4 - September 10th 2018
- [Scott Cochran] - Remove path to netstat binary since it can be in multiple places depending on host
## 0.5.3 - September 7th 2018
- [Scott Cochran] - Add port check to Command Watcher module
## 0.5.2 - September 7th 2018
- [Derrick Shultz] - Fix variable mismatch
## 0.5.1 - September 7th 2018
- [Derrick Shultz] - Update Command Watcher metric names
## 0.5.0 - September 5th 2018
- [Scott Cochran] - Add Command Watcher Monitor for netstat
## 0.4.0 - September 5th 2018
- [Derrick Shultz] - Add URL Monitor
## 0.3.9 - September 4th 2018
- [Derrick Shultz] - Promote JMX extension to prod, UTC and training
## 0.3.8 - August 30th 2018
- [Derrick Shultz] - Change JMX host to localhost to fix metric path
## 0.3.7 - August 24th 2018
- [Derrick Shultz] - Update JMX Monitor extension and monitors for mBeans with bool and string values
## 0.3.6 - August 23th 2018
- [Derrick Shultz] - Promote heap flags to all of production
## 0.3.5 - August 23nd 2018
- [Scott Cochran] - Remove inability to restart agents in production after hierarchy update completion.
## 0.3.4 - August 22nd 2018
- [Scott Cochran] - Update sysconfig with new hierarchy template.  Stop appd agent from restarting in production until all hierarchy updates are complete
## 0.3.3 - August 22th 2018
- [Derrick Shultz] - Source tier-config file for extra startup opts
## 0.3.2 - August 20th 2018
- [Derrick Shultz] - Repush automate
## 0.3.1 - August 20th 2018
- [Derrick Shultz] - Fix JMX template loop
## 0.3.0 - August 20th 2018
- [Derrick Shultz] - Add JMX extension
## 0.2.50 - August 8th 2018
- [Scott Cochran] Reverted previous changes
## 0.2.49 - August 7th 2018
- [Scott Cochran] - update sysconfig with new hierarchy template. Stop appd agent from restarting in production until all hierarchy updates are complete
## 0.2.48 - August 6th 2018
- [Derrick Shultz] - Only check for vmtoolsd in IADD1 and SJCD1
## 0.2.47 - August 6th 2018
- [Jason Roth] - Increase machine agent heap non-prod, iri, aqm, api
## 0.2.46 - August 6th 2018
- [Derrick Shultz] - Disable and exclude tst on Windows. Re-organize attributes into single hash. Don't defer extension config updates
## 0.2.45 - August 1st 2018
- [Derrick Shultz] - Increase machine agent heap non-prod, iri, aqm, api
## 0.2.44 - July 31st 2018
- [Derrick Shultz] - Disable and exclude on tst. Cleanup, foodcritic
## 0.2.43 - July 26th 2018
- [Derrick Shultz] - Fix Process and Logmonitor Extention configs
## 0.2.42 - July 8th 2018
- [Kevin Patel] -  Repush
## 0.2.41 - July 6th 2018
- [Kevin Patel] -  Process and Logmonitor Extention changes for Apps - Fix
## 0.2.40 - June 29th 2018
- [Kevin Patel] -  Process and Logmonitor Extention changes for Apps
## 0.2.39 - June 20th 2018
- [Kevin Patel] -  Change hierachy for all env except prod
## 0.2.38 - June 19th 2018
- [Kevin Patel] -  Change hierachy for dev and new log monitors, add df check
## 0.2.37 - June 19th 2018
- [Jason Roth ] -  add chef environment to hierarchy
## 0.2.36 - June 18th 2018
- [Kevin Patel ] - add logic to pull the logextention in the development env
## 0.2.35 - June 15th 2018
- [Jason Roth ] - changing from windows_zipfile resource to DSC archive resource
## 0.2.34 - June 14th 2018
- [Jason Roth ] - deploying to Prod in all datacenters
## 0.2.33 - June 13th 2018
- [Jason Roth ] - fixed order of attributes in default.rb
## 0.2.32 - June 12th 2018
- [Jason Roth ] - deploy agent to SJCD1 Prod
## 0.2.31 - June 11th 2018
- [Kevin Patel ] - change the start and restart logic for appdagent
## 0.2.30 - June 6th 2018
- [Kevin Patel ] - changed process monitor tempalte - fix
## 0.2.29 - June 5st  2018
- [Kevin Patel ] - changed process monitor tempalte
## 0.2.28 - June 1st  2018
- [Jason Roth] - change to node.default in recipe
## 0.2.27 - May 31rst  2018
- [Jason Roth] - limiting datacenters for windows
## 0.2.26 - May 31rst  2018
- [Jason Roth] - adding all lower environment servers
## 0.2.25 - May 30rd  2018
- [Kevin Patel] - update logmonitor & Processmonitor
## 0.2.24 - May 23rd  2018
- [Kevin Patel] - update logmonitor
## 0.2.23 - May 16th  2018
- [Kevin Patel] - Switch lower env from unknown to wid, add disable_agent flag
## 0.2.22 - May 16th  2018
- [Kevin Patel] - Restart Appd agent on all servers
## 0.2.21 - May 15th  2018
- [Kevin Patel] - Rollout to Appdyanmics to All Prodcution for Existing APM Servers
## 0.2.20 - May 14th  2018
- [Kevin Patel] - Rollout to Appdyanmics to SJCD1 and MKSD1 Existing APM Servers
## 0.2.19 - May 9th  2018
- [Kevin Patel] - Fix2 - Enable Machine Agent on staging and QA servers
## 0.2.18 - May 9th  2018
- [Kevin Patel] - Fix - Enable Machine Agent on staging and QA servers
## 0.2.17 - May 9th  2018
- [Kevin Patel] - Enable Machine Agent on staging and QA servers
## 0.2.16 - May 8th  2018
- [Kevin Patel] - Fix - Enable Machine Agent on QAP servers
## 0.2.15 - May 7th  2018
- [Kevin Patel] - Enable Machine Agent on QAP servers
## 0.2.14 - May 7th  2018
- [Kevin Patel] - Enable Machine Agent on all Dev & Intergration servers
## 0.2.13 - May 2nd  2018
- [Kevin Patel] - Enable Machine Agent on all Dev servers
## 0.2.12 - Apr 28th  2018
- [Kevin Patel] - Beta test Dev env wih the agent migration
## 0.2.11 - Apr 28th  2018
- [Kevin Patel] - Update the attribute to use os instead of Platform for Hiarchy
## 0.2.10 - Apr 27th  2018
- [Kevin Patel] -  update logmonior
## 0.2.9 - Apr 26th  2018
- [Kevin Patel] -  Remove the URLMonitor extention if installed, change extn configs
## 0.2.8 - Apr 25th  2018
- [Kevin Patel] -  Push Machine agent to Staging Production QA QAP Integration Perftest UTC Training
## 0.2.7 - Apr 24th  2018
- [Kevin Patel] -  Push Machine agent to SJC & MKSD1
## 0.2.6 - Apr 24th  2018
- [Kevin Patel] -  For Linux Change extention installation
## 0.2.4 - Apr 24th  2018
- [Jason Roth] -  add action :nothing
## 0.2.4 - Apr 24th  2018
- [Jason Roth] -  restart service on xml file update
## 0.2.3 - Apr 23rd  2018
- [Jason Roth] -  adding QA environment
## 0.2.2 - Apr 23rd  2018
- [Jason Roth] -  adding QA AD workgroups
## 0.2.1 - Apr 20th  2018
- [Jason Roth] -  initial testing complete
## 0.2.0 - Apr 19th  2018
- [Jason Roth] -  adding windows
## 0.1.11 - Apr 17th  2019
- [Kevin Patel] - Install machine agent on staging non-instrumented servers
## 0.1.10 - Apr 13th  2019
- [Kevin Patel] - Switch appagent to new agent if already exists
## 0.1.9 - Apr 10th  2019
- [Kevin Patel] -  upgrade to 4.4 version
## 0.1.8 - Apr 5rd 2019
- [Kevin Patel] -  enable and disable urlmonitors
## 0.1.7 - Apr 3rd 2019
- [Kevin Patel] -  Change the file name for urlmoniror
## 0.1.5 - Apr 3rd 2018
- [Kevin Patel] -  rollout to VA1 Business group SYS and CORP
## 0.1.2 to 0.1.4  - Apr 1st 2018
- [Kevin Patel] -  Testing Extentions
## 0.1.1 - Jan 27th 2018
- [Kevin Patel] -  Install Extentions

## 0.1.0 - Jan 26th 2018
- [Kevin Patel] - Initial version
