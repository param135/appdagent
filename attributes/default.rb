
case node['platform_family']
when 'rhel'
  default['ihg-appdagent']['install_dir'] = '/opt/appdynamics'
  default['ihg-appdagent']['monitoring_template'] = []
  default['ihg-appdagent']['machineagent']['version'] = '21.7.0.3160'
  # default['ihg-appdagent']['machineagent']['version'] = '4.5.4.1735' # Wrong mem calc
  # default['ihg-appdagent']['machineagent']['version'] = '4.5.2.1611' # Wrong mem calc
  default['ihg-appdagent']['machineagent']['install_dir'] = "#{node['ihg-appdagent']['install_dir']}/machine_agent/#{node['ihg-appdagent']['machineagent']['version']}"
  default['ihg-appdagent']['machineagent']['user'] = 'root'
  default['ihg-appdagent']['machineagent']['group'] = 'root'
  default['ihg-appdagent']['machineagent']['extensions'] = {
    LogMonitor: {
      default: true,
      pkg: 'LogMonitor-3.0.1.zip',
      conf_dir: '',
      default_conf: 'config.yml',
      app_monitors: [],
    },
    ProcessMonitor: {
      default: true,
      pkg: 'processmonitor-2.0.2.zip',
      conf_dir: '',
      default_conf: 'config.yml',
      app_monitors: [],
    },
    JMXMonitor: {
      default: false,
      pkg: 'JMXMonitor-v1.0.1.zip',
      conf_dir: '',
      default_conf: 'config.yml',
      app_monitors: {},
    },
    UrlMonitor: {
      default: false,
      pkg: 'urlmonitor-1.2.9.zip',
      conf_dir: '',
      default_conf: 'config.yml',
      app_monitors: [],
    },
    CommandWatcher: {
      default: false,
      pkg: 'commandwatcher-1.0.3.zip',
      conf_dir: '',
      default_conf: 'config.yml',
      app_monitors: [],
    },
    WMQMonitor: {
      default: false,
      pkg: 'wmqmonitor-7.0.1.zip',
      conf_dir: '',
      default_conf: 'config.yml',
      app_monitors: [],
    },
  }

  default['ihg-appdagent']['appagent_dir'] = "#{node['ihg-appdagent']['install_dir']}/oraclejava_agent/#{node['ihg-appdagent']['machineagent']['version']}"
when 'windows'
  default['ihg-appdagent']['machineagent']['version'] = '21.7.0.3160'
  # default['ihg-appdagent']['machineagent']['version'] = '4.4.3.1061'
  default['ihg-appdagent']['machineagent']['checksum'] = 'd87926426d6a9aaa24451b5127790f6000824a7c3dc2e612483587cc1ef2141c'
  default['ihg-appdagent']['install_dir'] = 'C:\appdynamics'
  default['ihg-appdagent']['monitor_dir'] = 'C:\appdynamics\monitors'
  default['ihg-appdagent']['custom_extension'] = {
    Name: 'IHGWindowsMonitor',
    ZipFile: 'IHGWindowsMonitor-1.1.0.zip',
  }
end

default['ihg-appdagent']['datacenter'] = node.attribute?('datacenter') && node['datacenter'].attribute?('name') ? node['datacenter']['name'] : ''
default['ihg-appdagent']['workgroup'] = node.attribute?('datacenter') && node['datacenter'].attribute?('workgroup') ? node['datacenter']['workgroup'] : ''
default['ihg-appdagent']['function'] = node.attribute?('datacenter') && node['datacenter'].attribute?('function') ? node['datacenter']['function'] : ''
default['ihg-appdagent']['repo_url'] = 'http://sdlcbrm.hiw.com/artifactory/ext-release-local/appdynamics'
default['ihg-appdagent']['machineagent']['pkg'] = "MachineAgent-#{node['ihg-appdagent']['machineagent']['version']}.zip"
default['ihg-appdagent']['machineagent']['pkg_url'] = "#{node['ihg-appdagent']['repo_url']}/#{node['ihg-appdagent']['machineagent']['pkg']}"
default['ihg-appdagent']['machineagent']['extension_config'] = 'http://sdlcscm.hiw.com/projects/CHSHARED/repos/ihg-appd-extensions/raw'
default['ihg-appdagent']['tier_name'] = ''
default['ihg-appdagent']['machineagent']['zip_path'] = "#{node['ihg-appdagent']['install_dir']}/#{node['ihg-appdagent']['machineagent']['pkg']}"

if node['ihg-appdagent']['workgroup'] == 'a05'
  default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor'] = {
    default: false,
    pkg: 'ApigeeMonitor-21.1.3.zip',
    conf_dir: '',
    default_conf: 'config.json',
    app_monitors: {},
  }
end

# These servers have too many monitoring templates
# Override until AppD lifts 95 character hierarchy path limit
fn = node['ihg-appdagent']['function']
fn = 'm9' if %w(sjcd1plaqmmq003.ihgint.global
                sjcd1plaqmmq004.ihgint.global
                iadd1plaqmmq003.ihgint.global
                iadd1plaqmmq004.ihgint.global).include?(node['fqdn'])

default['ihg-appdagent']['machineagent']['hierarchy_path'] = "#{node['ihg-appdagent']['datacenter']}|" \
                                                             "#{node.chef_environment}|" \
                                                             "#{node['os']}|" \
                                                             "#{node['ihg-appdagent']['workgroup']}|" \
                                                             "#{fn}|".downcase

bluestripe_wids = if node.attribute?('BlueStripe') && node['BlueStripe'].attribute?('allowed_wids') && !node['BlueStripe']['allowed_wids'].nil?
                    node['BlueStripe']['allowed_wids']
                  else
                    %w(
                      b2b bdc bbu b2g gru ari bas cdo cds cms cpi mar pre ras ava wqa avl avc bie
                      cps eco eve hgw pco c75 gls rd3 hgg res avc d01 aqs ord hcm hcg hss hcs pf1
                      pf2 pf3 pf4 pf5 rms rme eig f61 rmh hin ctc
                    )
                  end

default['ihg-appdagent']['monitor_bluestripe'] = if bluestripe_wids.include?(node['ihg-appdagent']['workgroup'])
                                                   true
                                                 else
                                                   false
                                                 end

case node.chef_environment
when 'Development', 'Acceptance', 'QAP'
  default['ihg-appdagent']['controller']['host'] = 'ihg-dev.saas.appdynamics.com'
  default['ihg-appdagent']['controller']['acc_name'] = 'ihg-dev'
  default['ihg-appdagent']['controller']['acc_key'] = 'f18432d83f8a'
  if node['ihg-appdagent']['workgroup'] == 'a05'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['org'] = 'ihg-dev'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['env'] = 'dev'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['credentials'] = 'YWRtaW5AaWhnLmNvbTpBcGlnZWUxMjM='
  end
when 'Integration'
  default['ihg-appdagent']['controller']['host'] = 'ihg-int.saas.appdynamics.com'
  default['ihg-appdagent']['controller']['acc_name'] = 'ihg-int'
  default['ihg-appdagent']['controller']['acc_key'] = '5x6tf8fed67g'
  if node['ihg-appdagent']['workgroup'] == 'a05'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['org'] = 'ihg-cert'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['env'] = 'int'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['credentials'] = 'YWRtaW5AaWhnLmNvbTpBcGlnZWUxMjMh'
  end
when 'QA', 'Staging', 'Perftest'
  default['ihg-appdagent']['controller']['host'] = 'ihg-stg.saas.appdynamics.com'
  default['ihg-appdagent']['controller']['acc_name'] = 'ihg-stg'
  default['ihg-appdagent']['controller']['acc_key'] = 'd87ba85fddc2'
  if node['ihg-appdagent']['workgroup'] == 'a05'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['org'] = 'ihg-cert'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['env'] = 'staging'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['credentials'] = 'QXBwYnVpbGRhbmRSZWxlYXNlQXV0b21hdGlvbkBpaGcuY29tOnhzd2VkYzEyMzQ='
  end
when 'Performance'
  default['ihg-appdagent']['controller']['host'] = 'ihg-perf.saas.appdynamics.com'
  default['ihg-appdagent']['controller']['acc_name'] = 'ihg-perf'
  default['ihg-appdagent']['controller']['acc_key'] = 'qdv5fq7oygh7'
  if node['ihg-appdagent']['workgroup'] == 'a05'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['env'] = 'perf'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['org'] = 'non-prod'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['credentials'] = 'QXBwYnVpbGRhbmRSZWxlYXNlQXV0b21hdGlvbkBpaGcuY29tOnphcXdzeDEyMzQ='
  end
when 'Production', 'UTC', 'Training'
  default['ihg-appdagent']['controller']['host'] = 'ihg-prod.saas.appdynamics.com'
  default['ihg-appdagent']['controller']['acc_name'] = 'ihg-prod'
  default['ihg-appdagent']['controller']['acc_key'] = 'e338a1515d7e'
  if node['ihg-appdagent']['workgroup'] == 'a05'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['org'] = 'ihg-prod'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['env'] = 'prod'
    default['ihg-appdagent']['machineagent']['extensions']['ApigeeMonitor']['app_monitors']['credentials'] = 'QXBwYnVpbGRhbmRSZWxlYXNlQXV0b21hdGlvbkBpaGcuY29tOnphcXdzeDEyMzQ='
  end
end
